import unittest
from unittest.mock import patch

from tire_pressure_monitoring import Alarm
from tire_pressure_monitoring import Sensor


class AlarmTest(unittest.TestCase):

    def setUp(self):
        self.alarm = Alarm()
        self.sensor = Sensor()
    def test_low_pressure_threshold_is_17(self):
        assert self.alarm._low_pressure_threshold == 17.0

    def test_high_pressure_threshold_is_21(self):
        assert self.alarm._high_pressure_threshold == 21.0

    def test_sensor_is_right_instance(self):
        self.assertTrue(isinstance(self.alarm._sensor, Sensor))

    def test_is_alarm_on_init_False(self):
        self.assertFalse(self.alarm.is_alarm_on)

    @patch('tire_pressure_monitoring.Sensor.pop_next_pressure_psi_value', return_value=16)
    def test_tire_pressure_too_low(self, pop_next_pressure_psi_value):
        self.alarm.check()
        self.assertTrue(self.alarm.is_alarm_on)

    @patch('tire_pressure_monitoring.Sensor.pop_next_pressure_psi_value', return_value=22)
    def test_tire_pressure_too_high(self, pop_next_pressure_psi_value):
        self.alarm.check()
        self.assertTrue(self.alarm.is_alarm_on)

    @patch('tire_pressure_monitoring.Sensor.pop_next_pressure_psi_value', return_value=17)
    def test_tire_pressure_low_limit(self, pop_next_pressure_psi_value):
        self.alarm.check()
        self.assertFalse(self.alarm.is_alarm_on)

    @patch('tire_pressure_monitoring.Sensor.pop_next_pressure_psi_value', return_value=21)
    def test_tire_pressure_high_limit(self, pop_next_pressure_psi_value):
        self.alarm.check()
        self.assertFalse(self.alarm.is_alarm_on)


if __name__ == "__main__":
    unittest.main()
